﻿namespace Sarlaft.Infraestructure.AZDigital.Integration
{
    using global::AZDigital.Domain.Entities.AZModels;
    using global::AZDigital.Domain.Entities.WebApi;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using RestSharp;
    using Sarlaft.Domain.Entities.Enum;
    using Sarlaft.Domain.Entities.Helper;
    using Sarlaft.Domain.Interfaces.Infraestructure.AZDigital;
    using System.Configuration;

    public class AZServiceIntegration: IAZServiceIntegration
    {
        public Domain.Entities.Models.ConnectionStrings ConnectionStrings { get; set; }

        public AZServiceIntegration(Domain.Entities.Models.ConnectionStrings ConnectionStrings)
        {
            this.ConnectionStrings = ConnectionStrings;
        }

        public JObject EnviarPeticionServicioIntegracionPost(AZModel aZModel)
        {
            aZModel.connectionStrings = new ConnectionStrings(this.ConnectionStrings.SarlaftConnectionString);

            string jsonToSend = JsonConvert.SerializeObject(aZModel);

            var client = new RestClient(ConfigurationManager.AppSettings["AZServiceIntegration"].ToString());
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", jsonToSend, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (response.Content != null)
            {
                return JsonConvert.DeserializeObject<JObject>(response.Content);
            }

            throw new SarlaftException("EnviarPeticionServicioIntegracionPost", ErrorMessagesEnum.AZDigitalUnavailable);
        }
    }
}
