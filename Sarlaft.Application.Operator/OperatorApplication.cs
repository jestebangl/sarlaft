﻿namespace Sarlaft.Application.Operator
{
    using Newtonsoft.Json;
    using Sarlaft.Domain.Entities.Models;
    using Sarlaft.Domain.Interfaces.Application.Operator;
    using Sarlaft.Domain.Interfaces.Infraestructure.Persistence;
    using System.Collections.Generic;
    using System.Linq;

    public class OperatorApplication : IOperatorApplication
    {
        public IPersistence AZDigitalDac { get; set; }

        public IOperatorHelper OperatorHelper { get; set; }

        public OperatorApplication(IPersistence AZDigitalDac,
            IOperatorHelper OperatorHelper)
        {
            this.AZDigitalDac = AZDigitalDac;
            this.OperatorHelper = OperatorHelper;
        }

        public long Add(AZProcessModel processModel, CreationModel creationModel)
        {
            AZDigitalModel digitalModel = new AZDigitalModel()
            {
                Json = JsonConvert.SerializeObject(processModel)
            };

            var response = AZDigitalDac.Add(digitalModel, creationModel.personalIdentification, (int)creationModel.personType);

            return OperatorHelper.GetId(response);
        }

        public AZDigitalModel GetByName(string personalIdentification, int personType)
        {
            var response = AZDigitalDac.GetByName(personalIdentification, personType);
            var results = OperatorHelper.GetFromDataSet(response);

            return results.Any() ?
                results.FirstOrDefault() :
                null;
        }

        public AZProcessModel GetById(long idAZDigital)
        {
            var response = AZDigitalDac.GetById(idAZDigital);
            var results = OperatorHelper.GetFromDataSet(response);

            if (results.Any())
            {
                return JsonConvert.DeserializeObject<AZProcessModel>(results.FirstOrDefault().Json);
            }

            return new AZProcessModel();
        }


        public void UploadDocument(UploadModel uploadModel, string documentId)
        {
            List<AZDigitalModel> related = this.GetAllByDocumentAndType(uploadModel.idAZDigital);

            AZProcessModel processModel = JsonConvert.DeserializeObject<AZProcessModel>(related.FirstOrDefault().Json);
            OperatorHelper.UpdateUploadModel(processModel, uploadModel, documentId);

            Update(related, processModel);
        }

        public UploadModel GetUploadedFile(AZProcessModel processModel, string documentId, string file)
        {
            AZFileProcessModel fileProcessModel = OperatorHelper.GetFileProcess(documentId, processModel);
            return new UploadModel()
            {
                directoryParentId = processModel.IdAzDigital,
                encodedFile = file,
                documentName = fileProcessModel.FileName,
                extension = fileProcessModel.Extension,
                version = fileProcessModel.Version,
                fileId = long.Parse(documentId)
            };
        }

        private List<AZDigitalModel> GetAllByDocumentAndType(long idAZDigital)
        {
            var response = AZDigitalDac.GetAllByDocumentAndType(idAZDigital);
            return OperatorHelper.GetFromDataSet(response);
        }

        public void MoveFile(MoveModel moveModel)
        {
            List<AZDigitalModel> relatedOld = this.GetAllByDocumentAndType(moveModel.idAZDigital);
            List<AZDigitalModel> relatedNew = this.GetAllByDocumentAndType(moveModel.idAZDigitalDestination);

            AZProcessModel processModelOld = JsonConvert.DeserializeObject<AZProcessModel>(relatedOld.FirstOrDefault().Json);
            AZProcessModel processModelNew = JsonConvert.DeserializeObject<AZProcessModel>(relatedNew.FirstOrDefault().Json);

            OperatorHelper.UpdateMoveModel(processModelOld, moveModel, processModelNew);

            Update(relatedOld, processModelOld);
            Update(relatedNew, processModelNew);
        }

        public void DeleteFile(DeleteModel deleteModel)
        {
            List<AZDigitalModel> related = this.GetAllByDocumentAndType(deleteModel.idAZDigital);

            AZProcessModel processModel = JsonConvert.DeserializeObject<AZProcessModel>(related.FirstOrDefault().Json);

            OperatorHelper.UpdateDeleteModel(processModel, deleteModel);

            Update(related, processModel);
        }

        private void Update(List<AZDigitalModel> digitalModels, AZProcessModel processModel)
        {
            foreach (var register in digitalModels)
            {
                AZDigitalDac.Update(register.IdAZDigital, JsonConvert.SerializeObject(processModel));
            }
        }
    }
}

