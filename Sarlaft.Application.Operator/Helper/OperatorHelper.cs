﻿namespace Sarlaft.Application.Operator.Helper
{
    using Sarlaft.Domain.Entities.Models;
    using Sarlaft.Domain.Interfaces.Application.Operator;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    public class OperatorHelper : IOperatorHelper
    {
        public OperatorHelper()
        {

        }

        public long GetId(DataSet ds)
        {
            var response = (from dt in ds.Tables[0].AsEnumerable()
                            select long.Parse(dt["IdAZDigital"].ToString())).FirstOrDefault();

            return response;
        }

        public List<AZDigitalModel> GetFromDataSet(DataSet ds)
        {
            var response = from dt in ds.Tables[0].AsEnumerable()
                           select new AZDigitalModel()
                           {
                               IdAZDigital = Convert.ToInt64(dt["IdAZDigital"]),
                               Json = Convert.ToString(dt["Json"]),
                               PersonType = Convert.ToInt32(dt["PersonType"]),
                               PersonIndentification = Convert.ToString(dt["PersonIntetification"]),
                               LastUpdate = Convert.ToDateTime(dt["LastUpdate"])
                           };

            return response.ToList();
        }

        public void UpdateUploadModel(AZProcessModel processModel, UploadModel uploadModel, string documentId)
        {
            foreach (var file in processModel.Files)
            {
                if (file.FileName.Equals(uploadModel.documentName.ToUpper()))
                {
                    file.Extension = uploadModel.extension;
                    file.IdAzDigital = documentId;
                    file.LastUpdate = DateTime.Now.ToString();
                    file.Version = string.IsNullOrEmpty(file.Version) ? "1" : (int.Parse(file.Version) + 1).ToString();
                }
            }
        }

        public void UpdateDeleteModel(AZProcessModel processModel, DeleteModel deleteModel)
        {
            foreach (var file in processModel.Files)
            {
                if (file.IdAzDigital.Equals(deleteModel.fileId.ToUpper()))
                {
                    file.Extension = string.Empty;
                    file.IdAzDigital = string.Empty; ;
                    file.LastUpdate = string.Empty;
                    file.Version = string.Empty;
                }
            }
        }

        public AZFileProcessModel GetFileProcess(string IdFileProcess, AZProcessModel processModel)
        {
            return processModel.Files.Where(x => x.IdAzDigital == IdFileProcess).FirstOrDefault();
        }

        public void UpdateMoveModel(AZProcessModel processModelOld, MoveModel moveModel, AZProcessModel processModelNew)
        {
            AZFileProcessModel fileProcessModel = new AZFileProcessModel();

            int indexOld = 0;
            int indexNew = 0;
            bool isFoundOld = false;
            bool isFoundNew = false;

            foreach (var file in processModelOld.Files)
            {
                if (file.IdAzDigital.Equals(moveModel.documentId))
                {
                    indexOld = processModelOld.Files.IndexOf(file);
                    isFoundOld = true;
                    break;
                }
            }

            foreach (var file in processModelNew.Files)
            {
                if (file.FileName.Equals(moveModel.newFileName))
                {
                    indexNew = processModelNew.Files.IndexOf(file);
                    isFoundNew = true;
                    break;
                }
            }

            if (isFoundOld && isFoundNew)
            {
                processModelNew.Files[indexNew].Extension = processModelOld.Files[indexOld].Extension;
                processModelNew.Files[indexNew].IdAzDigital = processModelOld.Files[indexOld].IdAzDigital;
                processModelNew.Files[indexNew].LastUpdate = processModelOld.Files[indexOld].LastUpdate;
                processModelNew.Files[indexNew].Version = processModelOld.Files[indexOld].Version;


                processModelOld.Files[indexOld].Extension = string.Empty;
                processModelOld.Files[indexOld].IdAzDigital = string.Empty;
                processModelOld.Files[indexOld].LastUpdate = string.Empty;
                processModelOld.Files[indexOld].Version = string.Empty;
            }
        }
    }
}
