﻿namespace Sarlaft.Domain.Entities.Logger
{
    using log4net;
    using log4net.Config;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class Logger : ILogger
    {
        /// <summary>
        /// The log.
        /// </summary>
        private static ILog log = LogManager.GetLogger(typeof(Logger));

        /// <summary>
        /// Initializes static members of the <see cref="Logger"/> class.
        /// </summary>
        static Logger()
        {
            XmlConfigurator.Configure();
        }

        /// <summary>
        /// Gets the current.
        /// </summary>
        public static ILogger Current { get { return new Logger(); } }

        /// <summary>
        /// The set logger.
        /// </summary>
        /// <param name="ilog">The log.</param>
        public static void SetLogger(ILog ilog)
        {
            log = ilog;
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Info(string message)
        {
            if (log.IsInfoEnabled)
            {
                log.Info(message);
            }
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Debug(string message)
        {
            if (log.IsDebugEnabled)
            {
                log.Debug(message);
            }
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Error(string message)
        {
            if (log.IsErrorEnabled)
            {
                log.Error(message);
            }
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        public void Error(string message, Exception ex)
        {
            if (log.IsErrorEnabled)
            {
                log.Error(message, ex);
            }
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Warn(string message)
        {
            if (log.IsWarnEnabled)
            {
                log.Warn(message);
            }
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        public void Warn(string message, Exception ex)
        {
            if (log.IsWarnEnabled)
            {
                log.Warn(message, ex);
            }
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Fatal(string message)
        {
            if (log.IsFatalEnabled)
            {
                log.Fatal(message);
            }
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        public void Fatal(string message, Exception ex)
        {
            if (log.IsFatalEnabled)
            {
                log.Fatal(message, ex);
            }
        }

        /// <summary>
        /// Sets the thread custom string properties.
        /// </summary>
        /// <param name="properties">The properties.</param>
        public void SetThreadCustomStringProperties(IDictionary<string, string> properties)
        {
            foreach (var property in properties)
            {
                LogicalThreadContext.Properties[property.Key] = property.Value;
            }
        }

        /// <summary>
        /// Sets the thread custom date time properties.
        /// </summary>
        /// <param name="properties">The properties.</param>
        public void SetThreadCustomDateTimeProperties(IDictionary<string, DateTime> properties)
        {
            foreach (var property in properties)
            {
                LogicalThreadContext.Properties[property.Key] = property.Value;
            }
        }

        /// <summary>
        /// Sets the general custom properties.
        /// </summary>
        /// <param name="properties">The properties.</param>
        public void SetGeneralCustomProperties(IDictionary<string, string> properties)
        {
            foreach (var property in properties)
            {
                GlobalContext.Properties[property.Key] = property.Value;
            }
        }

        /// <summary>
        /// Gets the current method.
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public string GetCurrentMethod()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);

            return sf.GetMethod().Name;
        }
    }
}
