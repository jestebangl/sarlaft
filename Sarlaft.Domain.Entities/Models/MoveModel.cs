﻿namespace Sarlaft.Domain.Entities.Models
{
    public class MoveModel
    {
        public long idAZDigital { get; set; }

        public long idAZDigitalDestination { get; set; }

        public string documentId { get; set; }

        public string destinyFolderId { get; set; }

        public string currentUser { get; set; }

        public string newFileName { get; set; }

        public MoveModel() { }
    }
}
