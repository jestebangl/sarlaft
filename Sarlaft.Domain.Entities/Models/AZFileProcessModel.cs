﻿namespace Sarlaft.Domain.Entities.Models
{
    public class AZFileProcessModel
    {
        public string IdAzDigital { get; set; }

        public string FileName { get; set; }

        public string Extension { get; set; }

        public string LastUpdate { get; set; }

        public string Version { get; set; }

        public AZFileProcessModel()
        {

        }
    }
}
