﻿namespace Sarlaft.Domain.Entities.Models
{
    public class ConnectionStrings
    {
        public string SarlaftConnectionString { get; set; }

        public ConnectionStrings() { }

        public ConnectionStrings(string SarlaftConnectionString, string BizuitConnection)
        {
            this.SarlaftConnectionString = SarlaftConnectionString;
        }

    }

}
