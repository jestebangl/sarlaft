﻿namespace Sarlaft.Domain.Entities.Models
{
    public class UploadModel
    {
        public long fileId { get; set; }

        public long idAZDigital { get; set; }

        public string directoryParentId { get; set; }

        public string documentName { get; set; }

        public string extension { get; set; }

        public string version { get; set; }

        public string currentUser { get; set; }

        public int fileSize { get; set; }

        public string encodedFile { get; set; }

        public UploadModel() { }
    }
}
