﻿namespace Sarlaft.Domain.Entities.Models
{
    using System.Collections.Generic;

    public class AZProcessModel
    {
        public string IdAzDigital { get; set; }

        public string DirectoryName { get; set; }

        public string IdAzDigitalParent { get; set; }

        public List<AZProcessModel> Subdirectories { get; set; }

        public List<AZFileProcessModel> Files { get; set; }

        public AZProcessModel()
        {

        }
    }
}
