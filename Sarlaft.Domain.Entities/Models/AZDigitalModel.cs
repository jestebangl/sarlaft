﻿namespace Sarlaft.Domain.Entities.Models
{
    using System;

    public class AZDigitalModel
    {
        public long IdAZDigital { get; set; }

        public string Json { get; set; }

        public DateTime LastUpdate { get; set; }

        public int PersonType { get; set; }

        public string PersonIndentification { get; set; }

        public AZDigitalModel()
        {

        }
    }
}
