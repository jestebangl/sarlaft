﻿namespace Sarlaft.Domain.Entities.Models
{
    using Sarlaft.Domain.Entities.Enum;


    public class CreationModel
    {
        public string personalIdentification { get; set; }

        public PersonTypeEnum personType { get; set; }

        public string currentUser { get; set; }

        public CreationModel()
        {

        }
    }
}
