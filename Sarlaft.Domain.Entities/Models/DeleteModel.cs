﻿namespace Sarlaft.Domain.Entities.Models
{
    public class DeleteModel
    {
        public long idAZDigital { get; set; }

        public string fileId { get; set; }

        public string currentUser { get; set; }
    }
}
