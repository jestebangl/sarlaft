﻿namespace Sarlaft.Domain.Entities.Models
{
    public class UploadedFileModel
    {
        public long idAZDigital { get; set; }

        public string documentId{ get; set; }

        public string currentUser { get; set; }
    }
}
