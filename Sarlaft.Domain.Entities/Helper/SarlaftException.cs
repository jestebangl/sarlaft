﻿namespace Sarlaft.Domain.Entities.Helper
{
    using Sarlaft.Domain.Entities.Enum;
    using System;
    using System.Runtime.Serialization;

    public class SarlaftException : Exception
    {
        public SarlaftException() : base() { }

        public SarlaftException(string message) : base(message)
        {
        }

        /// <summary>
        /// Thrown a new SarlaftException with a message for the user
        /// </summary>
        /// <param name="methodException">Current exception method</param>
        /// <param name="userExceptionMessage">Message to throw to the user</param>
        /// <param name="ex">Exception when ocurrs</param>
        public SarlaftException(string methodException, ErrorMessagesEnum userExceptionMessage, Exception ex = null)
        {
            if (ex != null)
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {ex.ToString()}");
            }
            else
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {userExceptionMessage.ToDescriptionString()}");
            }

            throw new SarlaftException(userExceptionMessage.ToDescriptionString());
        }

        /// <summary>
        /// Thrown a new GapException with a message for the user
        /// </summary>
        /// <param name="methodException">Current exception method</param>
        /// <param name="userExceptionMessage">Message to throw to the user</param>
        /// <param name="ex">Exception when ocurrs</param>
        public SarlaftException(string methodException, string userExceptionMessage, Exception ex = null)
        {
            if (ex != null)
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {ex.ToString()}");
            }
            else
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {userExceptionMessage}");
            }

            throw new SarlaftException(userExceptionMessage);
        }

        protected SarlaftException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
