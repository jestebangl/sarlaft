﻿namespace Sarlaft.Domain.Entities.Enum
{
    using System.ComponentModel;

    public enum ErrorMessagesEnum
    {
        [Description("Ocurrio un problema de conectividad con el servicio de AZ Digital. Intentelo de nuevo más tarde")]
        AZDigitalUnavailable = 1,
        [Description("No fue posible crear la estructura en AZ Digital, se realizó un rollback de la petición")]
        CreateStructureFailed = 2,
        [Description("El documento adjunto sobrepasa el máximo permitido.")]
        MaxSizePermited = 3,
        [Description("No fue posible cargar el documento, la respuesta del servicio fue: {0}")]
        UploadingError = 4,
        [Description("No se encontro una estructura relacionada en la base de datos para el id {0}")]
        StructureNotFound = 5,
        [Description("Ocurrió un error moviendo el documento. Error: {0}")]
        MovingElementError = 6,
        [Description("Ocurrió un error eliminando el documento. Error: {0}")]
        DeletingElementError = 6

    }

    public static partial class ErrorMessageEnumExtention
    {
        public static string ToDescriptionString(this ErrorMessagesEnum val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
