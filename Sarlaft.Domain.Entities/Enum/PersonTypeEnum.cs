﻿namespace Sarlaft.Domain.Entities.Enum
{
    using System;

    public enum PersonTypeEnum
    {
        Juridica = 1,
        Natural = 2

    }
}
