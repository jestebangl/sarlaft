﻿namespace Sarlaft.Service.Orchestrator
{
    using Newtonsoft.Json;
    using Sarlaft.Domain.Entities.Enum;
    using Sarlaft.Domain.Entities.Helper;
    using Sarlaft.Domain.Entities.Models;
    using Sarlaft.Domain.Interfaces.Application.AZDigital;
    using Sarlaft.Domain.Interfaces.Application.Operator;
    using Sarlaft.Domain.Interfaces.Service.Orchestrator;
    using System;

    public class OrchestratorService : IOrchestratorService
    {
        public IAZDigitalApplication AZDigitalApplication { get; set; }

        public IOperatorApplication OperatorApplication { get; set; }

        public OrchestratorService(IAZDigitalApplication AZDigitalApplication)
        {
            this.AZDigitalApplication = AZDigitalApplication;
        }

        public long CreaterAZStructure(CreationModel creationModel)
        {
            try
            {
                AZProcessModel processModel;

                AZDigitalModel digitalModel = OperatorApplication.GetByName(creationModel.personalIdentification, (int)creationModel.personType);
                if (digitalModel == null)
                {
                    processModel = AZDigitalApplication.CreateAZStructure(creationModel);
                }
                else
                {
                    processModel = JsonConvert.DeserializeObject<AZProcessModel>(digitalModel.Json);
                }

                return OperatorApplication.Add(processModel, creationModel);
            }
            catch (SarlaftException se)
            {
                throw se;
            }
            catch (Exception ex)
            {
                throw new SarlaftException("CreaterAZStructure", ErrorMessagesEnum.CreateStructureFailed, ex);
            }
        }

        public AZProcessModel GetStructureById(long idAZDigital)
        {
            try
            {
                return OperatorApplication.GetById(idAZDigital);
            }
            catch (SarlaftException se)
            {
                throw se;
            }
            catch (Exception ex)
            {
                throw new SarlaftException("GetStructureById", ErrorMessagesEnum.CreateStructureFailed, ex);
            }
        }

        public string UploadFile(UploadModel uploadModel)
        {
            try
            {
                AZProcessModel processModel = OperatorApplication.GetById(uploadModel.idAZDigital);

                if (!string.IsNullOrEmpty(processModel.IdAzDigital))
                {
                    string documentId = AZDigitalApplication.UploadFile(uploadModel, processModel.IdAzDigital);
                    OperatorApplication.UploadDocument(uploadModel, documentId);

                    return documentId;
                }

                throw new SarlaftException("UploadFile", ErrorMessagesEnum.StructureNotFound);
            }
            catch (SarlaftException se)
            {
                throw se;
            }
            catch (Exception ex)
            {
                throw new SarlaftException("GetStructureById", ErrorMessagesEnum.CreateStructureFailed, ex);
            }
        }

        public UploadModel GetUploadedFile(UploadedFileModel uploadedFileModel)
        {
            try
            {
                AZProcessModel processModel = OperatorApplication.GetById(uploadedFileModel.idAZDigital);
                string file = AZDigitalApplication.GetUploadedFile(uploadedFileModel.documentId, uploadedFileModel.currentUser);
                return OperatorApplication.GetUploadedFile(processModel, uploadedFileModel.documentId, file);
            }
            catch (SarlaftException se)
            {
                throw se;
            }
            catch (Exception ex)
            {
                throw new SarlaftException("GetUploadedFile", ErrorMessagesEnum.CreateStructureFailed, ex);
            }
        }

        public bool MoveFile(MoveModel moveModel)
         {
            try
            {
                bool response = AZDigitalApplication.MoveFile(moveModel);
                if (response)
                {
                    OperatorApplication.MoveFile(moveModel);
                }

                return response;
            }
            catch (SarlaftException se)
            {
                throw se;
            }
            catch (Exception ex)
            {
                throw new SarlaftException("MoveFile", ErrorMessagesEnum.CreateStructureFailed, ex);
            }
        }

        public bool DeleteFile(DeleteModel deleteModel)
        {
            try
            {
                bool response = AZDigitalApplication.DeleteFile(deleteModel);
                if (response)
                {
                    OperatorApplication.DeleteFile(deleteModel);
                }

                return response;
            }
            catch (SarlaftException se)
            {
                throw se;
            }
            catch (Exception ex)
            {
                throw new SarlaftException("MoveFile", ErrorMessagesEnum.CreateStructureFailed, ex);
            }
        }


    }
}
