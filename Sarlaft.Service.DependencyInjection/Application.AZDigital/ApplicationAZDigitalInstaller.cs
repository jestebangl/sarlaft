﻿namespace Sarlaft.Service.DependencyInjection.Application.AZDigital
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Sarlaft.Application.AZDigital.Bussines;
    using Sarlaft.Application.AZDigital.Helper;
    using Sarlaft.Domain.Interfaces.Application.AZDigital;

    public class ApplicationAZDigitalInstaller : IWindsorInstaller
    {
        public ApplicationAZDigitalInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
               Component.For<IAZDigitalHelper>().ImplementedBy<AZDigitalHelper>().Named("AZDigitalHelper")
                   .LifeStyle.PerWebRequest,

               Component.For<IAZDigitalApplication>().ImplementedBy<AZDigitalApplication>().Named("AZDigitalApplication")
                   .LifeStyle.PerWebRequest
                   );
        }
    }
}