﻿namespace Sarlaft.Service.DependencyInjection.Infraestructure.AZDigital
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Sarlaft.Domain.Entities.Models;
    using Sarlaft.Domain.Interfaces.Infraestructure.AZDigital;
    using Sarlaft.Infraestructure.AZDigital.Integration;


    public class InfraestructureAZDigitalInstaller : IWindsorInstaller
    {
        public ConnectionStrings ConnectionString { get; set; }

        public InfraestructureAZDigitalInstaller(ConnectionStrings ConnectionStrings)
        {
            this.ConnectionString = ConnectionStrings;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
               Component.For<IAZServiceIntegration>().ImplementedBy<AZServiceIntegration>().Named("AZServiceIntegration")
                   .DependsOn(Dependency.OnValue("ConnectionStrings", this.ConnectionString))
                   .LifeStyle.PerWebRequest
                   );
        }
    }
}