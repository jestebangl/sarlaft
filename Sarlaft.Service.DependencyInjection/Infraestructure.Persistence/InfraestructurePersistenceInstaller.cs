﻿namespace Sarlaft.Service.DependencyInjection.Infraestructure.Persistence
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Sarlaft.Domain.Entities.Models;
    using Sarlaft.Domain.Interfaces.Infraestructure.Persistence;
    using Sarlaft.Infraestructure.Persistence.Dac;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class InfraestructurePersistenceInstaller : IWindsorInstaller
    {
        public ConnectionStrings ConnectionString { get; set; }

        public InfraestructurePersistenceInstaller(ConnectionStrings ConnectionStrings)
        {
            this.ConnectionString = ConnectionStrings;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
               Component.For<IPersistence>().ImplementedBy<AZDigitalDac>().Named("AZDigitalDac")
                   .DependsOn(Dependency.OnValue("ConnectionStrings", this.ConnectionString))
                   .LifeStyle.PerWebRequest
                   );
        }
    }
}