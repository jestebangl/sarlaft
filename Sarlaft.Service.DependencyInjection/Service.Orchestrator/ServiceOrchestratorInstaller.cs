﻿namespace Sarlaft.Service.DependencyInjection.Service.OrchestratorService
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Sarlaft.Domain.Interfaces.Service.Orchestrator;
    using Sarlaft.Service.Orchestrator;

    public class ServiceOrchestratorInstaller : IWindsorInstaller
    {
        public ServiceOrchestratorInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
               Component.For<IOrchestratorService>().ImplementedBy<OrchestratorService>().Named("OrchestratorService")
                   .LifeStyle.PerWebRequest
                   );
        }
    }
}