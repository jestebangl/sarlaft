﻿namespace Sarlaft.Service.DependencyInjection.Application.Operator
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Sarlaft.Application.Operator;
    using Sarlaft.Application.Operator.Helper;
    using Sarlaft.Domain.Interfaces.Application.Operator;


    public class ApplicationOperatorInstaller : IWindsorInstaller
    {
        public ApplicationOperatorInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
               Component.For<IOperatorHelper>().ImplementedBy<OperatorHelper>().Named("OperatorHelper")
                   .LifeStyle.PerWebRequest,

               Component.For<IOperatorApplication>().ImplementedBy<OperatorApplication>().Named("OperatorApplication")
                   .LifeStyle.PerWebRequest
                   );
        }
    }
}