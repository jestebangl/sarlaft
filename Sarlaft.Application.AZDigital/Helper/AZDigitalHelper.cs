﻿namespace Sarlaft.Application.AZDigital.Helper
{
    using Newtonsoft.Json;
    using Sarlaft.Domain.Entities.Enum;
    using Sarlaft.Domain.Entities.Helper;
    using Sarlaft.Domain.Entities.Models;
    using Sarlaft.Domain.Interfaces.Application.AZDigital;
    using System.Configuration;
    using System.IO;
    using System.Linq;

    public class AZDigitalHelper : IAZDigitalHelper
    {
        public AZDigitalHelper()
        {

        }

        public AZProcessModel GetStringJSonParametrization()
        {
            using (StreamReader r = new StreamReader(ConfigurationManager.AppSettings["Template"].ToString()))
            {
                var readFile = r.ReadToEnd();

                return JsonConvert.DeserializeObject<AZProcessModel>(readFile);
            }
        }

        public void ValidateFileSize(int fileSize)
        {
            long sizeAllowd = long.Parse(ConfigurationManager.AppSettings["SizeAllow"]);
            if ((fileSize / 1024) >= sizeAllowd)
            {
                throw new SarlaftException("ValidateFileSize", ErrorMessagesEnum.MaxSizePermited);
            }
        }

        public string GetAZDigitalParentId(PersonTypeEnum personTypeEnum)
        {
            switch (personTypeEnum)
            {
                case PersonTypeEnum.Juridica:
                    return ConfigurationManager.AppSettings["IdAZDigitalPersonaJuridica"].ToString();
                case PersonTypeEnum.Natural:
                    return ConfigurationManager.AppSettings["IdAZDigitalPersonaNatural"].ToString();
                default:
                    return string.Empty;
            }
        }
    }
}
