﻿namespace Sarlaft.Application.AZDigital.Bussines
{
    using global::AZDigital.Domain.Entities.AZModels;
    using global::AZDigital.Domain.Entities.Helpers;
    using global::AZDigital.Domain.Entities.WebApi;
    using Newtonsoft.Json.Linq;
    using Sarlaft.Domain.Entities.Enum;
    using Sarlaft.Domain.Entities.Helper;
    using Sarlaft.Domain.Entities.Models;
    using Sarlaft.Domain.Interfaces.Application.AZDigital;
    using Sarlaft.Domain.Interfaces.Infraestructure.AZDigital;
    using System.Configuration;

    public class AZDigitalApplication : IAZDigitalApplication
    {
        public IAZServiceIntegration AZServiceIntegration { get; set; }

        public IAZDigitalHelper AZDigitalHelper { get; set; }

        public AZDigitalApplication(IAZServiceIntegration AZServiceIntegration,
            IAZDigitalHelper AZDigitalHelper)
        {
            this.AZServiceIntegration = AZServiceIntegration;
            this.AZDigitalHelper = AZDigitalHelper;
        }

        public AZProcessModel CreateAZStructure(CreationModel creationModel)
        {
            AZProcessModel processModel = AZDigitalHelper.GetStringJSonParametrization();
            processModel.DirectoryName = creationModel.personalIdentification;
            processModel.IdAzDigitalParent = AZDigitalHelper.GetAZDigitalParentId(creationModel.personType);


            AZModel aZModel = AZParametrizationHelper.GetCreateDirectoryModel(EnumeratorHelper.DirectoryType.CarpetaPrivada,
                processModel.DirectoryName,
                processModel.IdAzDigitalParent,
                ConfigurationManager.AppSettings["AZUserName"].ToString());

            aZModel.processModel = new ProcessModel();
            aZModel.processModel.stageId = 1;
            aZModel.processModel.user = creationModel.currentUser;

            JObject serviceResponse = AZServiceIntegration.EnviarPeticionServicioIntegracionPost(aZModel);

            if (serviceResponse != null)
            {
                if (serviceResponse["Estado"] != null && serviceResponse["Estado"].Value<string>().Equals("Ok"))
                {
                    processModel.IdAzDigital = serviceResponse["NuevoDiId"].Value<string>();
                    return processModel;
                }
                else if (serviceResponse["faultstring"] != null)
                {
                    if (serviceResponse["directoryId"] != null)
                    {
                        processModel.IdAzDigital = serviceResponse["directoryId"].Value<string>();
                        return processModel;
                    }

                    throw new SarlaftException("CreaterAZStructure", $"Error: {serviceResponse["faultstring"].Value<string>()}");
                }
            }

            throw new SarlaftException("CreaterAZStructure", ErrorMessagesEnum.AZDigitalUnavailable);
        }


        public string UploadFile(UploadModel uploadModel, string directoryParentId)
        {
            AZDigitalHelper.ValidateFileSize(uploadModel.fileSize);

            AZModel aZModel = AZParametrizationHelper.GetUploadFileModel(directoryParentId,
                $"{uploadModel.documentName.ToUpper()}.{uploadModel.extension.ToUpper()}",
                uploadModel.encodedFile);

            aZModel.processModel = new ProcessModel()
            {
                stageId = 1,
                user = uploadModel.currentUser
            };


            JObject serviceResponse = AZServiceIntegration.EnviarPeticionServicioIntegracionPost(aZModel);

            if (serviceResponse != null)
            {
                if (serviceResponse["Estado"] != null && serviceResponse["Estado"].Value<string>().Equals("Ok"))
                {
                    return serviceResponse["NuevoArId"].Value<string>();
                }

                throw new SarlaftException("UploadFile", string.Format(ErrorMessagesEnum.UploadingError.ToDescriptionString(), serviceResponse["faultstring"].Value<string>())); ;
            }

            throw new SarlaftException("UploadFile", ErrorMessagesEnum.AZDigitalUnavailable);
        }

        public string GetUploadedFile(string fileIdAZDigital, string currentUser)
        {
            AZModel aZModel = AZParametrizationHelper.GetRequestFileModel(fileIdAZDigital);
            aZModel.processModel = new ProcessModel()
            {
                stageId = 1,
                user = currentUser
            };

            JObject serviceResponse = AZServiceIntegration.EnviarPeticionServicioIntegracionPost(aZModel);
            return serviceResponse["Archivo"].Value<string>();
        }

        public bool MoveFile(MoveModel moveModel)
        {
            AZModel aZModel = AZParametrizationHelper.GetMoveModel("Archivo",
               moveModel.documentId,
               moveModel.destinyFolderId,
               "NO");

            aZModel.processModel = new ProcessModel()
            {
                stageId = 1,
                user = moveModel.currentUser
            };

            JObject serviceResponse = AZServiceIntegration.EnviarPeticionServicioIntegracionPost(aZModel);

            if (serviceResponse["Estado"] != null && serviceResponse["Estado"].Value<string>().Equals("Ok"))
            {
                return true;
            }

            throw new SarlaftException(string.Format(ErrorMessagesEnum.MovingElementError.ToDescriptionString(), serviceResponse["faultstring"].Value<string>()));
        }

        public bool DeleteFile(DeleteModel deleteModel)
        {
            AZModel aZModel = AZParametrizationHelper.GetMoveModel("Archivo",
               deleteModel.fileId,
               ConfigurationManager.AppSettings["DeleteFolderId"],
               "NO");

            aZModel.processModel = new ProcessModel()
            {
                stageId = 1,
                user = deleteModel.currentUser
            };

            JObject serviceResponse = AZServiceIntegration.EnviarPeticionServicioIntegracionPost(aZModel);

            if (serviceResponse["Estado"] != null && serviceResponse["Estado"].Value<string>().Equals("Ok"))
            {
                return true;
            }

            throw new SarlaftException(string.Format(ErrorMessagesEnum.DeletingElementError.ToDescriptionString(), serviceResponse["faultstring"].Value<string>()));
        }
    }


}
