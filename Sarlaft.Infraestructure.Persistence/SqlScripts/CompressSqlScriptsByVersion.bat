@echo OFF

SETLOCAL ENABLEDELAYEDEXPANSION

set BEGIN_INSERT=INSERT INTO [dbo].[LogScripts] (ScriptName, DateTime, Version, PCName) VALUES (
set END_INSERT=)
set DATE_TIME=GETDATE()
set HOST_NAME=HOST_NAME()

echo Validating whether directory "CompressedScripts" exists and created it...
if not exist "CompressedScripts" mkdir CompressedScripts
echo Directory "CompressedScripts" created if it does not exist.

for /d %%D in (*) do (
	
	set DIRECTORY=%%~nxD
	set PATH=%%~fD
	
	IF !DIRECTORY! == CompressedScripts (
		echo Directory "!DIRECTORY!" not processed.	
	)
	IF !DIRECTORY! == ConfiguracionInicialIFactura (
		echo Directory "!DIRECTORY!" not processed.		
	) ELSE (
		echo Processing directory "!DIRECTORY!"...
				
		echo Removing the version file "!DIRECTORY!" when exists...
		del CompressedScripts\!DIRECTORY!.sql
		echo File "!DIRECTORY!" removed if it does not exist.
		echo Compressing the version file "!DIRECTORY!" ...
		
		for %%I in (!DIRECTORY!\*.*) do (
			set FILE_NAME=%%~nxI 
			echo Adding !FILE_NAME!...
			
			type !DIRECTORY!\!FILE_NAME! >> CompressedScripts\!DIRECTORY!.sql
			
			echo.>> CompressedScripts\!DIRECTORY!.sql
			echo.>> CompressedScripts\!DIRECTORY!.sql
		)
		
		echo Version !DIRECTORY! file compressed.
	)
	echo ------------------------------------------------------------
)

pause
echo Terminado