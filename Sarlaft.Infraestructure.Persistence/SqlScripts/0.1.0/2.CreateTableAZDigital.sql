IF NOT EXISTS(SELECT * FROM sys.objects WHERE name LIKE 'AZDigital')
BEGIN 
	CREATE TABLE AZDigital(
		IdAZDigital BIGINT IDENTITY(1,1) PRIMARY KEY,
		[Json] VARCHAR(MAX) NOT NULL,
		[PersonType] INT,
		[PersonIntetification] VARCHAR(50),
		LastUpdate DATETIME NOT NULL
	);
END
GO

