IF NOT EXISTS(SELECT * FROM sys.databases WHERE name LIKE 'SarlaftDB')
BEGIN
	CREATE DATABASE SarlaftDB;
END
GO



IF NOT EXISTS(SELECT * FROM sys.objects WHERE name LIKE 'AZDigital')
BEGIN 
	CREATE TABLE AZDigital(
		IdAZDigital BIGINT IDENTITY(1,1) PRIMARY KEY,
		[Json] VARCHAR(MAX) NOT NULL,
		[PersonType] INT,
		[PersonIntetification] VARCHAR(50),
		LastUpdate DATETIME NOT NULL
	);
END
GO



/****** Object:  Table [dbo].[LogAZDigital]    Script Date: 09/08/2019 9:45:20 a. m. ******/
DROP TABLE [dbo].[LogAZDigital]
GO

/****** Object:  Table [dbo].[LogAZDigital]    Script Date: 09/08/2019 9:45:21 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LogAZDigital](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IdBandeja] [int] NULL,
	[Entreda] [nvarchar](max) NULL,
	[Salida] [nvarchar](max) NULL,
	[RegistradoPor] [nvarchar](50) NULL,
	[FechaRegistro] [smalldatetime] NULL,
	[Tipo] [nvarchar](50) NULL,
 CONSTRAINT [PK_RF_LogAZDigital] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO




