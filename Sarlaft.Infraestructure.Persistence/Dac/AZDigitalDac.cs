﻿namespace Sarlaft.Infraestructure.Persistence.Dac
{
    using Sarlaft.Domain.Entities.Models;
    using Sarlaft.Domain.Interfaces.Infraestructure.Persistence;
    using System.Data;
    using Transfiriendo.Core.AccessToData;

    public class AZDigitalDac : IPersistence
    {
        public ConnectionStrings ConnectionStrings { get; set; }

        public AZDigitalDac(ConnectionStrings ConnectionStrings)
        {
            this.ConnectionStrings = ConnectionStrings;
        }

        public DataSet Get()
        {
            throw new System.NotImplementedException();
        }

        public DataSet GetById(long idAZDigital)
        {
            Dac dac = new Dac(this.ConnectionStrings.SarlaftConnectionString);
            var result = dac.SqlQueryResult("GetAZDigitalById",
                new ModelDac("IdAZDigital", SqlDbType.BigInt, idAZDigital));

            return result;
        }

        public DataSet Add(AZDigitalModel aZDigitalModel, string personalIdentification, int personType)
        {
            Dac dac = new Dac(this.ConnectionStrings.SarlaftConnectionString);
            var result = dac.SqlQueryResult("AddAZDigital",
                new ModelDac("json", SqlDbType.VarChar, aZDigitalModel.Json),
                new ModelDac("personalIdentification", SqlDbType.VarChar, personalIdentification),
                new ModelDac("personType", SqlDbType.Int, personType));

            return result;
        }

        public bool Delete(AZDigitalModel aZDigitalModel)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(long idAZDigital, string json)
        {
            Dac dac = new Dac(this.ConnectionStrings.SarlaftConnectionString);
            var result = dac.SqlUpdate("UpdateAZDigital",
                new ModelDac("IdAZDigital", SqlDbType.BigInt, idAZDigital),
                new ModelDac("json", SqlDbType.VarChar, json));

            return result;
        }

        public DataSet GetByName(string personalIdentification, int personType)
        {
            Dac dac = new Dac(this.ConnectionStrings.SarlaftConnectionString);
            var result = dac.SqlQueryResult("GetAZDigitalByName",
                new ModelDac("PersonIdentification", SqlDbType.VarChar, personalIdentification),
                new ModelDac("PersonType", SqlDbType.Int, personType));

            return result;
        }

        public DataSet GetAllByDocumentAndType(long idAZDigital)
        {
            Dac dac = new Dac(this.ConnectionStrings.SarlaftConnectionString);
            var result = dac.SqlQueryResult("GetAllByDocumentAndType",
                new ModelDac("IdAZDigital", SqlDbType.BigInt, idAZDigital));

            return result;
        }



    }
}
