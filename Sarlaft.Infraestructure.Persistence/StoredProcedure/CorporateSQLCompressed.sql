SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddAZDigital]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AddAZDigital] AS' 
END
GO
ALTER PROCEDURE [dbo].[AddAZDigital] @json VARCHAR(MAX),
									 @personalIdentification VARCHAR(50),
									 @personType INT
AS
BEGIN
	INSERT INTO dbo.AZDigital([Json], [LastUpdate],[PersonIntetification],[PersonType]) 
	VALUES (@json, (SELECT GETDATE()), @personalIdentification, @personType);
	SELECT @@IDENTITY as IdAZDigital;
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IngresarLogAzDigital]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IngresarLogAzDigital] AS' 
END
GO
ALTER PROCEDURE [dbo].[IngresarLogAzDigital]
	@idBandeja int
	,@entrada NVARCHAR(MAX)
	,@salida NVARCHAR(MAX)
	,@registradoPor NVARCHAR(50)
	,@tipo NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [dbo].[LogAZDigital]
           ([IdBandeja]
           ,[Entreda]
           ,[Salida]
           ,[RegistradoPor]
           ,[FechaRegistro]
		   ,Tipo)
     VALUES
           (@idBandeja
           ,@entrada
           ,@salida
           ,@registradoPor
           ,GETDATE()
		   ,@tipo)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAllByDocumentAndType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetAllByDocumentAndType] AS' 
END
GO
ALTER PROCEDURE [dbo].[GetAllByDocumentAndType] @IdAZDigital BIGINT
AS
BEGIN
	DECLARE @PersonType INT;
	DECLARE @PersonIntetification VARCHAR(50);

	SELECT @PersonType = PersonType,
		@PersonIntetification = PersonIntetification
	FROM AZDigital
	WHERE IdAZDigital = @idAZDigital;

	SELECT * 
	FROM AZDigital
	WHERE @PersonIntetification=PersonIntetification
	AND @PersonType=PersonType
	ORDER BY LastUpdate DESC;

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAZDigitalById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetAZDigitalById] AS' 
END
GO
ALTER PROCEDURE [dbo].[GetAZDigitalById] @IdAZDigital BIGINT
AS
BEGIN
	SELECT *
	FROM dbo.AZDigital
	WHERE [IdAZDigital]=@IdAZDigital;
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAZDigitalByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetAZDigitalByName] AS' 
END
GO
ALTER PROCEDURE [dbo].[GetAZDigitalByName] @PersonIdentification VARCHAR(50),
										   @PersonType INT
AS
BEGIN
	SELECT TOP 1 *
	FROM dbo.AZDigital
	WHERE [PersonType]=@PersonType 
	AND	[PersonIntetification] LIKE @PersonIdentification
	ORDER BY [LastUpdate] DESC;
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateAZDigital]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateAZDigital] AS' 
END
GO
ALTER PROCEDURE [dbo].[UpdateAZDigital] @IdAZDigital BIGINT,
										@json VARCHAR(MAX)
AS
BEGIN
	UPDATE AZDigital
	SET [Json]=@json,
	LastUpdate = (SELECT GETDATE())
	WHERE IdAZDigital = @IdAZDigital;
END
GO

