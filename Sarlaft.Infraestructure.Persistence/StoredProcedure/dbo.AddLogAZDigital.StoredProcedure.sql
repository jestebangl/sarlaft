SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IngresarLogAzDigital]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IngresarLogAzDigital] AS' 
END
GO
ALTER PROCEDURE [dbo].[IngresarLogAzDigital]
	@idBandeja int
	,@entrada NVARCHAR(MAX)
	,@salida NVARCHAR(MAX)
	,@registradoPor NVARCHAR(50)
	,@tipo NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [dbo].[LogAZDigital]
           ([IdBandeja]
           ,[Entreda]
           ,[Salida]
           ,[RegistradoPor]
           ,[FechaRegistro]
		   ,Tipo)
     VALUES
           (@idBandeja
           ,@entrada
           ,@salida
           ,@registradoPor
           ,GETDATE()
		   ,@tipo)
END
GO


