SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAZDigitalByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetAZDigitalByName] AS' 
END
GO
ALTER PROCEDURE [dbo].[GetAZDigitalByName] @PersonIdentification VARCHAR(50),
										   @PersonType INT
AS
BEGIN
	SELECT TOP 1 *
	FROM dbo.AZDigital
	WHERE [PersonType]=@PersonType 
	AND	[PersonIntetification] LIKE @PersonIdentification
	ORDER BY [LastUpdate] DESC;
END
GO

