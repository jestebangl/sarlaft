SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateAZDigital]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateAZDigital] AS' 
END
GO
ALTER PROCEDURE [dbo].[UpdateAZDigital] @IdAZDigital BIGINT,
										@json VARCHAR(MAX)
AS
BEGIN
	UPDATE AZDigital
	SET [Json]=@json,
	LastUpdate = (SELECT GETDATE())
	WHERE IdAZDigital = @IdAZDigital;
END
GO

