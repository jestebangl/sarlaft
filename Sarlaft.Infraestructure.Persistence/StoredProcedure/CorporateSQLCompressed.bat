@ECHO OFF

echo Deleting existing compressed SQL File ...
echo ---------------------------------------------------
del CorporateSQLCompressed.sql
echo ---------------------------------------------------
echo Compressing SQL Files ...
echo ---------------------------------------------------
copy *.sql CorporateSQLCompressed.sql
echo ---------------------------------------------------
pause
echo Done.