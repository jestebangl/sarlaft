SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAllByDocumentAndType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetAllByDocumentAndType] AS' 
END
GO
ALTER PROCEDURE [dbo].[GetAllByDocumentAndType] @IdAZDigital BIGINT
AS
BEGIN
	DECLARE @PersonType INT;
	DECLARE @PersonIntetification VARCHAR(50);

	SELECT @PersonType = PersonType,
		@PersonIntetification = PersonIntetification
	FROM AZDigital
	WHERE IdAZDigital = @idAZDigital;

	SELECT * 
	FROM AZDigital
	WHERE @PersonIntetification=PersonIntetification
	AND @PersonType=PersonType
	ORDER BY LastUpdate DESC;

END
GO

