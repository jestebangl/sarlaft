SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddAZDigital]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AddAZDigital] AS' 
END
GO
ALTER PROCEDURE [dbo].[AddAZDigital] @json VARCHAR(MAX),
									 @personalIdentification VARCHAR(50),
									 @personType INT
AS
BEGIN
	INSERT INTO dbo.AZDigital([Json], [LastUpdate],[PersonIntetification],[PersonType]) 
	VALUES (@json, (SELECT GETDATE()), @personalIdentification, @personType);
	SELECT @@IDENTITY as IdAZDigital;
END
GO



