﻿namespace SarlaftWebApi.Controllers
{
    using Sarlaft.Domain.Entities.Models;
    using Sarlaft.Domain.Interfaces.Service.Orchestrator;
    using System.Web.Http;


    public class DocumentHandlerController : ApiController
    {
        public IOrchestratorService OrchestratorService { get; set; }

        public DocumentHandlerController(IOrchestratorService OrchestratorService)
        {
            this.OrchestratorService = OrchestratorService;
        }

        [HttpPost]
        [Route("api/documenthandler/createazdigitalstructure")]
        public IHttpActionResult CreateAZDigitalStructure(CreationModel creationModel)
        {
            return Ok(OrchestratorService.CreaterAZStructure(creationModel));
        }

        [HttpGet]
        [Route("api/documenthandler/getstructurebyid")]
        public IHttpActionResult GetStructureById(long idAZDigital)
        {
            return Ok(OrchestratorService.GetStructureById(idAZDigital));
        }

        [HttpPost]
        [Route("api/documenthandler/uploadfile")]
        public IHttpActionResult UploadFile(UploadModel uploadModel)
        {
            return Ok(OrchestratorService.UploadFile(uploadModel));
        }

        [HttpPost]
        [Route("api/documenthandler/getuploadedfile")]
        public IHttpActionResult GetUploadedFile(UploadedFileModel uploadedFileModel)
        {
            return Ok(OrchestratorService.GetUploadedFile(uploadedFileModel));
        }

        [HttpPost]
        [Route("api/documenthandler/movefile")]
        public IHttpActionResult MoveFile(MoveModel moveModel)
        {
            return Ok(OrchestratorService.MoveFile(moveModel));
        }

        [HttpPost]
        [Route("api/documenthandler/deletefile")]
        public IHttpActionResult DeleteFile(DeleteModel deleteModel)
        {
            return Ok(OrchestratorService.DeleteFile(deleteModel));
        }


    }
}
