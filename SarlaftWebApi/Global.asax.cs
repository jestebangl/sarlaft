namespace SarlaftWebApi
{
    using Castle.MicroKernel.Lifestyle;
    using Castle.MicroKernel.Resolvers.SpecializedResolvers;
    using Sarlaft.Domain.Entities.Models;
    using Sarlaft.Service.DependencyInjection.Application.AZDigital;
    using Sarlaft.Service.DependencyInjection.Application.Operator;
    using Sarlaft.Service.DependencyInjection.Infraestructure.AZDigital;
    using Sarlaft.Service.DependencyInjection.Infraestructure.Persistence;
    using Sarlaft.Service.DependencyInjection.Service.OrchestratorService;
    using SarlaftWebApi.Windsor;
    using System.Configuration;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;


    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            InitializeIoC(System.Web.Http.GlobalConfiguration.Configuration);
        }

        public static void InitializeIoC(HttpConfiguration configuration)
        {
            ConnectionStrings connectionStrings = new ConnectionStrings()
            {
                SarlaftConnectionString = ConfigurationManager.ConnectionStrings["SarlaftConnectionString"].ToString()
            };

            var windsor = Ragolo.Core.IoC.IocHelper.Instance;
            var contenedor = windsor.GetContainer();


            //INFRAESTRUCTURE
            windsor.Install(new InfraestructureAZDigitalInstaller(connectionStrings));
            windsor.Install(new InfraestructurePersistenceInstaller(connectionStrings));

            //APPLICATION
            windsor.Install(new ApplicationAZDigitalInstaller());
            windsor.Install(new ApplicationOperatorInstaller());

            //SERVICE
            windsor.Install(new ServiceOrchestratorInstaller());

            //WEB API
            windsor.Install(new Windsor.Installer());


            WindsorControllerFactory controllerFactory = new WindsorControllerFactory(contenedor.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            contenedor.Kernel.Resolver.AddSubResolver(new CollectionResolver(contenedor.Kernel, true));
            contenedor.BeginScope();
            var dependencyResolver = new WindsorDependencyResolver(contenedor);
            configuration.DependencyResolver = dependencyResolver;

        }
    }
}
