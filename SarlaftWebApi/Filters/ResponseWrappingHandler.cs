﻿namespace SarlaftWebApi.Filters
{
    using Newtonsoft.Json;
    using Sarlaft.Domain.Entities.Helper;
    using Sarlaft.Domain.Entities.Models;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http;

    public class ResponseWrappingHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            string str = request.Content.ReadAsStringAsync().Result;
            var decriptedFromJavascript = str;

            IEnumerable<string> esCifrado = new List<string>();

            request.Headers.TryGetValues("EsCifrado", out esCifrado);

            if (esCifrado != null && bool.Parse(esCifrado.FirstOrDefault()) == true && !string.IsNullOrEmpty(str))
            {
                decriptedFromJavascript = EncriptationHelper.DecryptAes256(str, ConfigurationManager.AppSettings["webApiPassCipher"]);
            }

            request.Content = new StringContent(decriptedFromJavascript);
            request.Content.Headers.ContentType.MediaType = "application/json";

            var response = await base.SendAsync(request, cancellationToken);

            return BuildApiResponse(request, response);
        }

        /// <summary>The build api response.</summary>
        /// <param name="request">The request.</param>
        /// <param name="response">The response.</param>
        /// <returns>The <see cref="HttpResponseMessage"/>.</returns>
        private HttpResponseMessage BuildApiResponse(HttpRequestMessage request, HttpResponseMessage response)
        {
            bool showError = true;

            object content;
            List<string> modelStateErrors = new List<string>();
            var success = true;

            if (response.TryGetContentValue(out content) && !response.IsSuccessStatusCode)
            {
                HttpError error = content as HttpError;
                if (error != null)
                {
                    content = null;
                    success = false;
                    var httpErrorObject = response.Content.ReadAsStringAsync().Result;
                    if (error.ModelState != null)
                    {

                        var anonymousErrorObject = new { message = "", ModelState = new Dictionary<string, string[]>() };

                        var deserializedErrorObject = JsonConvert.DeserializeAnonymousType(
                            httpErrorObject,
                            anonymousErrorObject);

                        var modelStateValues = deserializedErrorObject.ModelState.Select(kvp => string.Join(". ", kvp.Value));

                        for (int i = 0; i < modelStateValues.Count(); i++)
                        {
                            modelStateErrors.Add(modelStateValues.ElementAt(i));
                        }
                    }
                    else if (!response.IsSuccessStatusCode)
                    {
                        //response.StatusCode = (content == null) ? System.Net.HttpStatusCode.NoContent : response.StatusCode;       
                        var anonymousErrorObject = new { Message = string.Empty, ExceptionMessage = string.Empty };
                        var deserializedErrorObject = JsonConvert.DeserializeAnonymousType(httpErrorObject, anonymousErrorObject);

                        modelStateErrors.Add((showError) ?
                            !string.IsNullOrWhiteSpace(deserializedErrorObject.ExceptionMessage)
                                ? deserializedErrorObject.ExceptionMessage
                                : deserializedErrorObject.Message
                                : deserializedErrorObject.Message);
                    }
                }
            }

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                modelStateErrors.Add("Credenciales inválidas.");
                success = false;
            }


            string contentSerialize = JsonConvert.SerializeObject(content);
            var contentBase64 = System.Text.Encoding.UTF8.GetBytes(contentSerialize);
            var contentBase64String = Convert.ToBase64String(contentBase64);

            IEnumerable<string> esCifrado = new List<string>();
            var newResponse = new HttpResponseMessage();

            request.Headers.TryGetValues("EsCifrado", out esCifrado);

            if (esCifrado != null && bool.Parse(esCifrado.FirstOrDefault()) == true)
            {
                newResponse = request.CreateResponse(response.StatusCode, new WebApiResponse(EncriptationHelper.EncryptAes256(contentBase64String, ConfigurationManager.AppSettings["webApiPassCipher"]), modelStateErrors, success));
            }
            else
            {
                newResponse = request.CreateResponse(response.StatusCode, new WebApiResponse(content, modelStateErrors, success));
            }


            foreach (var header in response.Headers)
            {
                newResponse.Headers.Add(header.Key, header.Value);
            }

            return newResponse;
        }

    }
}