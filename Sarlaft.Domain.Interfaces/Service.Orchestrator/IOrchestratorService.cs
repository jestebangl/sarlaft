﻿namespace Sarlaft.Domain.Interfaces.Service.Orchestrator
{
    using Sarlaft.Domain.Entities.Models;

    public interface IOrchestratorService
    {
        long CreaterAZStructure(CreationModel creationModel);

        AZProcessModel GetStructureById(long idAZDigital);

        string UploadFile(UploadModel uploadModel);

        UploadModel GetUploadedFile(UploadedFileModel uploadedFileModel);

        bool MoveFile(MoveModel moveModel);

        bool DeleteFile(DeleteModel deleteModel);
    }
}
