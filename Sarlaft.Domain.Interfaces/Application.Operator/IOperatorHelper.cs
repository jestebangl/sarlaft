﻿namespace Sarlaft.Domain.Interfaces.Application.Operator
{
    using Sarlaft.Domain.Entities.Models;
    using System.Collections.Generic;
    using System.Data;


    public interface IOperatorHelper
    {
        long GetId(DataSet ds);

        List<AZDigitalModel> GetFromDataSet(DataSet ds);

        void UpdateUploadModel(AZProcessModel processModel, UploadModel uploadModel, string documentId);

        AZFileProcessModel GetFileProcess(string IdFileProcess, AZProcessModel processModel);

        void UpdateMoveModel(AZProcessModel processModelOld, MoveModel moveModel, AZProcessModel processModelNew);

        void UpdateDeleteModel(AZProcessModel processModel, DeleteModel deleteModel);
    }
}
