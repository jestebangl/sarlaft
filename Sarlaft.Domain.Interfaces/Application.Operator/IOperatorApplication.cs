﻿namespace Sarlaft.Domain.Interfaces.Application.Operator
{
    using Sarlaft.Domain.Entities.Models;


    public interface IOperatorApplication
    {
        long Add(AZProcessModel processModel, CreationModel creationModel);

        AZDigitalModel GetByName(string personalIdentification, int personType);

        AZProcessModel GetById(long idAZDigital);

        void UploadDocument(UploadModel uploadModel, string documentId);

        UploadModel GetUploadedFile(AZProcessModel processModel, string documentId, string file);

        void MoveFile(MoveModel moveModel);

        void DeleteFile(DeleteModel deleteModel);
    }
}
