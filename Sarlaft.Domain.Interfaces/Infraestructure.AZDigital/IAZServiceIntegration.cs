﻿namespace Sarlaft.Domain.Interfaces.Infraestructure.AZDigital
{
    using global::AZDigital.Domain.Entities.AZModels;
    using Newtonsoft.Json.Linq;

    public interface IAZServiceIntegration
    {
        JObject EnviarPeticionServicioIntegracionPost(AZModel aZModel);
    }
}
