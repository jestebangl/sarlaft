﻿namespace Sarlaft.Domain.Interfaces.Application.AZDigital
{
    using Sarlaft.Domain.Entities.Models;

    public interface IAZDigitalApplication
    {
        AZProcessModel CreateAZStructure(CreationModel creationModel);

        string UploadFile(UploadModel uploadModel, string directoryParentId);

        string GetUploadedFile(string fileIdAZDigital, string currentUser);

        bool MoveFile(MoveModel moveModel);

        bool DeleteFile(DeleteModel deleteModel);
    }
}
