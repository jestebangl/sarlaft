﻿namespace Sarlaft.Domain.Interfaces.Application.AZDigital
{
    using Sarlaft.Domain.Entities.Enum;
    using Sarlaft.Domain.Entities.Models;


    public interface IAZDigitalHelper
    {
        AZProcessModel GetStringJSonParametrization();

        void ValidateFileSize(int fileSize);

        string GetAZDigitalParentId(PersonTypeEnum personTypeEnum);
    }
}
