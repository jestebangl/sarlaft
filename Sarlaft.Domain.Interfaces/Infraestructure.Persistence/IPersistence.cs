﻿namespace Sarlaft.Domain.Interfaces.Infraestructure.Persistence
{
    using Sarlaft.Domain.Entities.Models;
    using System.Data;


    public interface IPersistence
    {
        DataSet Get();

        DataSet GetById(long idAZDigital);

        DataSet GetByName(string personalIdentification, int personType);

        bool Delete(AZDigitalModel aZDigitalModel);

        bool Update(long idAZDigital, string json);

        DataSet GetAllByDocumentAndType(long idAZDigital);

        DataSet Add(AZDigitalModel aZDigitalModel, string personalIdentification, int personType);

    }
}
